#pragma config(Sensor, S1,     SSide,          sensorEV3_Ultrasonic)
#pragma config(Sensor, S3,     SForward,       sensorEV3_Ultrasonic)
#pragma config(Motor,  motorA,          LeftMotor,     tmotorEV3_Large, PIDControl, encoder)
#pragma config(Motor,  motorB,          RightMotor,    tmotorEV3_Large, PIDControl, encoder)
/////////////////////////////////////
//**Robot's dimentions
/////////////////////////////////////
//Distance between wheels
#define LM 15
//Right/Left US offset by 'x' relative to center
#define LSU -1.5f
//Right/Left US offset by 'y' relative to wheels
#define LFM 14

/////////////////////////////////////
//**Ultrasonic sensor's coeffs
/////////////////////////////////////
//Max distance, that our US can return
#define US_MAX_DIST 100
#define NORMAL_WALL_DIST 12
#define FRONT_SENSOR_MIN_DIST 1
#define FRONT_SENSOR_BORDER_DIST 20

/////////////////////////////////////
//**Motors
/////////////////////////////////////
#define MOTOR_NORMAL_SPEED 100 //50
#define MOTOR_MAX_POWER 100
#define MOTOR_NORMAL_MIN_POWER 34 //32
#define MOTOR_EXTRA_MIN_POWER -100

/////////////////////////////////////
//**Coeffs
/////////////////////////////////////
#define PCOEFF 8
#define DCOEFF 3
//Front coeff for non-linear algorithm
#define FRONT_COEFF 50
//Min value, that coeff for linear algorithm can have
#define FRONT_LINEAR_COEFF_FLOOR 141
//I don't know
//DO NOT TOUCH THAT!
#define FRONT_LINEAR_COEFF_SOMETHING 150

/////////////////////////////////////
//**MACROS
/////////////////////////////////////
#define CLIP(x, floor, ceil) ((x > ceil ? ceil : (x < floor ? floor : x)))
#define HOLE(x, floor) (x < floor ? 0 : x)
#define IS_LESS(var1, var2, yes, no) (var1 < var2 ? yes : no)

/////////////////////////////////////
//**I HAVE NEVER USED IT BEFORE
//**SO, I DON'T KNOW HOW TO CALL IT
/////////////////////////////////////
#define USE_PD
#define USE_P
//#define USE_D
#define USE_FRONT_SENSOR
#define USE_LINEAR_FRONT_COEFF
//#define USE_SENSOR_ITERATION_ALG

//Num if iterations for US filter(?)
#define NUM_OF_ITERATIONS 1

//Returns value from US in sm
float GetSensorValue(char sensor)
{

  sensorReset(sensor);

#ifdef USE_SENSOR_ITERATION_ALG

  static float SensorVar;
  SensorVar = 0;
  for(int i = 0; i < NUM_OF_ITERATIONS; i++)
  {
    SensorVar += getIRDistance(sensor);
  }
  return SensorVar / NUM_OF_ITERATIONS;

#endif //USE_SENSOR_ITERATION_ALG

  return getIRDistance(sensor);
}

task main()
{

   int LTimeWait = 0;

  float Error = 0;
  float Delta = 0;

  float PrevError = 0;

  float SideSensorDist = 0;
  float PrevSideSensorDist = 0;
  float FrontSensorDist = FRONT_SENSOR_MIN_DIST;
  float FrontSensorCoeff = 0;
  int MotorMinPower = MOTOR_NORMAL_MIN_POWER;

  int LeftMotorPower = 0;
  int RightMotorPower = 0;

  bool HasChanged = false;

  //int MotorMaxPower = 100;
  do
  {


#ifdef USE_PD

  SideSensorDist = getUSDistance(SSide);
  HasChanged = ((SideSensorDist != PrevSideSensorDist) ? true : false);
  PrevSideSensorDist = SideSensorDist;

#ifdef USE_FRONT_SENSOR

    FrontSensorDist = CLIP(getUSDistance(SForward), FRONT_SENSOR_MIN_DIST, US_MAX_DIST);
    MotorMinPower = IS_LESS(FrontSensorDist, FRONT_SENSOR_BORDER_DIST, MOTOR_EXTRA_MIN_POWER, MOTOR_NORMAL_MIN_POWER);

#ifdef USE_LINEAR_FRONT_COEFF

    FrontSensorCoeff = HOLE((FRONT_LINEAR_COEFF_SOMETHING - FrontSensorDist), FRONT_LINEAR_COEFF_FLOOR);

#else //USE_LINEAR_FRONT_COEFF

    //!!!THIS IS NOT A LINEAR ALGORITHM!!!
    FrontSensorCoeff = 1/ FrontSensorDist;
    FrontSensorCoeff = FrontSensorCoeff * FRONT_COEFF;
    FrontSensorCoeff = HOLE((FrontSensorCoeff * FrontSensorCoeff), 1);

#endif //!USE_LINEAR_FRONT_COEFF

    SideSensorDist = SideSensorDist - FrontSensorCoeff;

#endif //USE_FRONT_SENSOR

#ifdef USE_P

	  Error = PCOEFF*(SideSensorDist -  NORMAL_WALL_DIST);

#endif //USE_P

#ifdef USE_D

    if(HasChanged)
    {
      Delta = DCOEFF * (Error - PrevError);
	    PrevError = Error;
    }


#endif //USE_D

#endif// USE_PD

	if(LTimeWait < 500)
	{

		delay(1);
		LTimeWait++;
		continue;
	}


    LeftMotorPower = CLIP(MOTOR_NORMAL_SPEED + (Error + Delta), MotorMinPower, MOTOR_MAX_POWER-MotorMinPower );
    RightMotorPower = CLIP(MOTOR_NORMAL_SPEED - (Error + Delta), MotorMinPower, MOTOR_MAX_POWER-MotorMinPower);

		setMotorSpeed(motorA, LeftMotorPower);
		setMotorSpeed(motorB, RightMotorPower);

	}while(1);

}